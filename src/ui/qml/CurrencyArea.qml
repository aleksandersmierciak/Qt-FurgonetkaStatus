import QtQuick 2.1

import QtQuick.XmlListModel 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

ColumnLayout {
    id: currencyArea

    property double eur
    property double usd

    onVisibleChanged: if (currencyArea.visible == true) core.requestExchangeRates()

    visible: false
    opacity: 0
    spacing: 10

    Connections {
        target: core
        onExchangeRatesReceived: {
            console.log("Updated currency rates")
            eur = core.getExchangeRate("eur")
            usd = core.getExchangeRate("usd")

            placeholderLabel.visible = false
            dataColumn.visible = true
        }
    }

    Label {
        id: placeholderLabel

        visible: true
        text: "Awaiting reply from Furgonetka..."
    }

    ColumnLayout {
        id: dataColumn

        visible: false
        spacing: 10

        Label {
            id: euroLabel
            text: "Euro: 1 € = " + currencyArea.eur + " zł"
        }

        Label {
            id: usdLabel
            text: "Dollar: 1 $ = " + currencyArea.usd + " zł"
        }
    }

    states: State {
        name: "active"
        PropertyChanges { target: currencyArea; visible: true; opacity: 1 }
    }

    transitions: Transition {
        NumberAnimation { properties: "opacity"; duration: 400; easing.type: Easing.InOutCubic }
    }
}
