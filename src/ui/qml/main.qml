import QtQuick 2.1
import QtQuick.Controls 1.0

Rectangle {
    id: root

    width: 450; height: 450

    StackView {
        id: stackView

        anchors.fill: parent
        initialItem: Qt.resolvedUrl("MainPage.qml")

        delegate: StackViewDelegate {
            function transitionFinished(properties)
            {
                properties.exitItem.opacity = 1
            }

            property Component pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                    duration: 200
                }
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                    duration: 800
                }
            }
        }
    }

    gradient: Gradient {
        GradientStop { position: 0.0; color: "#33CCFF" }
        GradientStop { position: 0.4; color: "#FFFFFF" }
    }
}
