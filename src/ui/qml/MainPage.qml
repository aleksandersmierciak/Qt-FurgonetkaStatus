import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Item {
    id: mainPage

    Rectangle {
        id: banner

        anchors { top: parent.top; topMargin: 10; horizontalCenter: parent.horizontalCenter }
        width: 380; height: 100
        color: "transparent"; border.color: "black"
    }

    Label {
        id: loggedInLabel

        anchors { top: banner.bottom; topMargin: 10; right: parent.right; rightMargin: 10 }
        text: qsTr("Logged in as ") + core.email
        visible: false
    }

    Column {
        id: mainMenu
        anchors { top: loggedInLabel.bottom; topMargin: 10; horizontalCenter: parent.horizontalCenter }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Button {
                id: loginButton

                text: qsTr("Login")

                onClicked: mainMenu.state = "login"
            }
            Button {
                id: registerButton

                text: qsTr("Register")

                onClicked: {
                    mainMenu.state = "default"
                    stackView.push(Qt.resolvedUrl("RegisterPage.qml"))
                }
            }
        }

        LoginArea {
            id: loginArea

            state: "inactive"
            Layout.alignment: Qt.AlignHCenter
        }

        Button {
            id: findButton

            text: qsTr("Find a package")
            Layout.alignment: Qt.AlignHCenter

            onClicked: mainMenu.state = "find"
        }

        FindArea {
            id: findArea
        }

        Button {
            id: currencyButton

            text: qsTr("Currency rates")
            Layout.alignment: Qt.AlignHCenter

            onClicked: mainMenu.state = "currency"
        }

        CurrencyArea {
            id: currencyArea

            anchors.horizontalCenter: parent.horizontalCenter
        }



        move: Transition {
            NumberAnimation { properties: "y"; easing.type: Easing.InOutCubic }
        }

        states: [
            State {
                name: "login"
                PropertyChanges { target: registerButton; opacity: 0.3 }
                PropertyChanges { target: findButton; opacity: 0.3 }
                PropertyChanges { target: currencyButton; opacity: 0.3 }
                PropertyChanges { target: loginArea; state: "active" }
            },
            State {
                name: "find"
                PropertyChanges { target: loginButton; opacity: 0.3 }
                PropertyChanges { target: registerButton; opacity: 0.3 }
                PropertyChanges { target: currencyButton; opacity: 0.3 }
                PropertyChanges { target: findArea; state: "active" }
            },
            State {
                name: "currency"
                PropertyChanges { target: loginButton; opacity: 0.3 }
                PropertyChanges { target: registerButton; opacity: 0.3 }
                PropertyChanges { target: findButton; opacity: 0.3 }
                PropertyChanges { target: currencyArea; state: "active" }
            }
        ]
    }

    states: State {
        name: "loggedIn"
        PropertyChanges { target: loggedInLabel; visible: true; text: "Logged in as " + core.email }
    }

    transitions: Transition {
        NumberAnimation { properties: "opacity"; duration: 400; easing.type: Easing.InOutCubic }
    }

    Label {
        id: termsLink

        anchors { bottom: parent.bottom; bottomMargin: 10; horizontalCenter: parent.horizontalCenter }
        text: qsTr("Terms of usage")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                mainMenu.state = "default"
                stackView.push(Qt.resolvedUrl("RegisterPage.qml"))
            }
        }
    }
}
