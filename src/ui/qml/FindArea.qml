import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

RowLayout {
    id: findArea

    visible: false
    opacity: 0
    anchors.horizontalCenter: parent.horizontalCenter

    Label {
        id: findLabel

        text: "Package number:"
    }

    TextField {
        id: findField

        width: 90
        placeholderText: "Package number"
    }

    states: State {
        name: "active"
        PropertyChanges { target: findField; focus: true }
        PropertyChanges { target: findArea; visible: true; opacity: 1 }
    }

    transitions: Transition {
        NumberAnimation { properties: "opacity"; duration: 400; easing.type: Easing.InOutCubic }
    }
}
