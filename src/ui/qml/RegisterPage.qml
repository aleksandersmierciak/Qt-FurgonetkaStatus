import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

/*!
  \qmltype RegisterPage
  \inqmlmodule ForgunetkaStatusUi
  \brief RegisterPage is an Item consisting of input elements for data required to register in Furgonetka service.

  This component is intended to use in an application implementing StackView.
*/
Item {
    id: registerPage

    property var parameters

    Component.onCompleted: core.requestCaptcha()

    Connections {
        target: core
        onCaptchaReceived: {
            captchaImage.source = "data:image/gif;base64," + core.getCaptcha()
            console.log("Updated Captcha image")
        }
    }

    ColumnLayout {
        anchors.fill: parent

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            GridLayout {
                columns: 2; columnSpacing: 10

                Image {
                    id: captchaImage

                    Layout.rowSpan: 2
                    Layout.alignment: Qt.AlignHCenter

                    width: 100; height: 50
                }

                Label {
                    text: qsTr("Captcha code:")
                }

                TextField {
                    id: codeField
                    placeholderText: qsTr("Captcha code")

                    width: 300
                }

                Label {
                    text: qsTr("Partner id:")
                }

                TextField {
                    id: partnerIdField

                    placeholderText: qsTr("Partner id")
                }

                Label {
                    text: qsTr("Account type:")
                }

                RowLayout {
                    ExclusiveGroup {
                        id: accountTypeGroup
                    }

                    RadioButton {
                        id: isPerson

                        checked: true
                        exclusiveGroup: accountTypeGroup
                        text: qsTr("Person")
                    }

                    RadioButton {
                        id: isCompany

                        exclusiveGroup: accountTypeGroup
                        text: qsTr("Company")
                    }
                }

                Label {
                    text: qsTr("E-mail address:")
                }

                TextField {
                    id: emailField

                    validator: RegExpValidator { regExp: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/ }
                    placeholderText: qsTr("E-mail address")
                }

                Label {
                    text: qsTr("Password:")
                }

                TextField {
                    id: passwordField

                    placeholderText: qsTr("Password")
                    echoMode: TextInput.Password
                }

                Label {
                    text: qsTr("Repeat password:")
                }

                TextField {
                    id: password2Field

                    placeholderText: qsTr("Repeat password")
                    echoMode: TextInput.Password
                }

                Label {
                    text: qsTr("Company name:")
                    visible: isCompany.checked
                }

                TextField {
                    id: companyField

                    placeholderText: qsTr("Company name")
                    visible: isCompany.checked
                }

                Label {
                    text: qsTr("NIP (PL or EU):")
                    visible: isCompany.checked
                }

                TextField {
                    id: nipField

                    placeholderText: qsTr("NIP")
                    visible: isCompany.checked
                }

                Label {
                    text: qsTr("First name:")
                }

                TextField {
                    id: nameField

                    placeholderText: qsTr("First name")
                }

                Label {
                    text: qsTr("Surname:")
                }

                TextField {
                    id: surnameField

                    placeholderText: qsTr("Surname")
                }

                Label {
                    text: qsTr("Address:")
                }

                TextField {
                    id: addressField

                    placeholderText: qsTr("Address")
                }

                Label {
                    text: qsTr("City:")
                }

                TextField {
                    id: cityField

                    placeholderText: qsTr("City")
                }

                Label {
                    text: qsTr("Postcode:")
                }

                TextField {
                    id: postcodeField

                    placeholderText: qsTr("Postcode")
                }

                Label {
                    text: qsTr("Phone number:")
                }

                TextField {
                    id: phoneField

                    placeholderText: qsTr("Phone number")
                }

                Label {
                    text: qsTr("IBAN:")
                }

                TextField {
                    id: ibanField

                    placeholderText: qsTr("IBAN")
                }

                CheckBox {
                    id: commercialCheck
                    text: qsTr("accept newsletter")
                }

                CheckBox {
                    id: regulationsCheck
                    text: qsTr("accept terms of usage")
                }


                transitions: Transition {
                    NumberAnimation { properties: "opacity"; duration: 400; easing.type: Easing.InOutCubic }
                }

        //        move: Transition {
        //            NumberAnimation { properties: "y"; easing.type: Easing.InOutCubic }
        //        }
            }
        }

        RowLayout {
            Button {
                id: backButton
                text: qsTr("Back")

                onClicked: stackView.pop()
            }

            Button {
                id: registerButton
                text: qsTr("Register")

                onClicked: {
                    parameters = {
                        "code": codeField.text,
                        "partnerId": partnerIdField.text,
                        "accountType": isPerson ? "personal" : "company",
                        "email": emailField.text,
                        "password": passwordField.text,
                        "password2": password2Field.text,
                        "name": nameField.text,
                        "surname": surnameField.text,
                        "street": addressField.text,
                        "postcode": postcodeField.text,
                        "city": cityField.text,
                        "phone": phoneField.text,
                        "iban": ibanField.text,
                        "company": companyField.text,
                        "nip": nipField.text,
                        "regulations": regulationsCheck.checked,
                        "commercial": commercialCheck.checked
                    }
                    core.requestUserRegister(parameters)
                }
            }
        }
    }
}
