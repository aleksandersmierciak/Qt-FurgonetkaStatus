import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Item {
    id: loginArea

    visible: false; opacity: 0

    ColumnLayout {
        GridLayout {
            columns: 2; columnSpacing: 10

            Label {
                id: loginLabel

                text: qsTr("Login:")
            }

            TextField {
                id: loginField

                width: 90
                placeholderText: qsTr("Login")
                KeyNavigation.backtab: enterButton
                KeyNavigation.tab: passwordField
            }

            Label {
                id: passwordLabel

                text: qsTr("Password:")
            }

            TextField {
                id: passwordField

                width: 90
                echoMode: TextInput.Password
                placeholderText: qsTr("Password")
                KeyNavigation.backtab: loginField
                KeyNavigation.tab: rememberMeField
            }
        }

        RowLayout {
            //spacing: -15
            CheckBox {
                id: rememberMeField

                text: qsTr("Remember me")
                KeyNavigation.backtab: passwordField
                KeyNavigation.tab: enterButton
            }

            Button {
                id: enterButton

                action: logInAction
                width: 30
                KeyNavigation.backtab: rememberMeField
                KeyNavigation.tab: loginField
                isDefault: true
            }
        }

        Action {
            id: logInAction

            text: "↵"
            shortcut: "Enter"

            onTriggered: core.requestUserLogIn(
                             loginField.text,
                             Qt.md5(passwordField.text),
                             rememberMeField.checked
                         )
        }
    }

    states: State {
        name: "active"
        PropertyChanges { target: loginField; focus: true }
        PropertyChanges { target: loginArea; visible: true; opacity: 1 }
    }

    transitions: Transition {
        NumberAnimation { properties: "opacity"; duration: 400; easing.type: Easing.InOutCubic }
    }
}

