#include <QApplication>
#include <QQmlContext>

#include <qtquick2applicationviewer.h>
#include <settings.h>
#include <core.h>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName(QStringLiteral("asmierciak"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("asmierciak.com"));
    QCoreApplication::setApplicationName(QStringLiteral("Furgonetka Status"));

    Settings *settings = new Settings(&app);

    Core core(settings, &app);

    QtQuick2ApplicationViewer viewer;
    viewer.rootContext()->setContextProperty(QStringLiteral("settings"), settings);
    viewer.rootContext()->setContextProperty(QStringLiteral("core"), &core);

    viewer.setMainQmlFile(QStringLiteral("qml/main.qml"));
    viewer.setMinimumSize(QSize(450, 450));
    viewer.setResizeMode(QtQuick2ApplicationViewer::SizeRootObjectToView);
    viewer.showExpanded();

    return app.exec();
}
