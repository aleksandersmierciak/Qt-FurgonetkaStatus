#ifndef CORE_H
#define CORE_H

#include <QJsonObject>
#include <QMap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QString>
#include <QUrl>
#include <QVariantMap>

#include <settings.h>
#include <useraccount.h>
#include <companyaccount.h>

enum class Destination;

class Core : public QObject {
    Q_OBJECT

public:
    Core(Settings *settings, QObject *parent = 0);

    Q_INVOKABLE void requestCaptcha(const int width = 100, const int height = 50) const;
    Q_INVOKABLE QString getCaptcha() const;

    Q_INVOKABLE void requestUserRegister(const QVariantMap &parameterMap) const;
    Q_INVOKABLE void requestUserLogIn() const;
    Q_INVOKABLE void requestUserLogIn(const QString &email, const QString &password, const bool autoLogin = false) const;
    Q_INVOKABLE void requestUserInfo() const;
    Q_INVOKABLE void requestUserEdit(const QVariantMap &parameters) const;
    Q_INVOKABLE void requestUserPay(const double amount) const;
    Q_INVOKABLE void requestUserMessage(const int subject, const QString &content) const;

    Q_INVOKABLE void requestExchangeRates() const;
    Q_INVOKABLE double getExchangeRate(const QString &currency) const;
    Q_INVOKABLE void requestPackstations(const QString &postcode) const;

    Q_INVOKABLE void requestPackageTrack(const int packageNo) const;

    Q_INVOKABLE void requestPackageAdd(const QVariantMap &parameterMap) const;
    Q_INVOKABLE void requestPackageCancel(const int packageId) const;
    Q_INVOKABLE void requestPackagePrice(const QVariantMap &parameterMap) const;
    Q_INVOKABLE void requestPackageDelete(const int packageId) const;
    Q_INVOKABLE void requestPackageDetails(const int packageNo) const;

    Q_INVOKABLE void requestPackagesSent(const QVariantMap &parameterMap) const;
    Q_INVOKABLE void requestPackagesWaiting() const;
    Q_INVOKABLE void requestPackagesOrder(const QList<int> &packageIds) const;

    Q_INVOKABLE void requestAddressbookList(const QVariantMap &parameterMap) const;
    Q_INVOKABLE void requestAddressbookAdd(const QVariantMap &parameterMap) const;
    Q_INVOKABLE void requestAddressbookDelete(const int deletedId) const;

signals:
    void captchaReceived();

    void userRegistered();
    void userLoggedIn();
    void userInfoReceived();
    void userEdited();
    void userPaid();
    void userMessageSent();

    void exchangeRatesReceived();
    void packstationsReceived();

    void packageTracked();

    void packageAdded();
    void packageCanceled();
    void packagePriced();
    void packageDeleted();
    void packageDetailed();

    void packagesSentReceived();
    void packagesWaitingReceived();
    void packagesOrdered();

    void addressbookListed();
    void addressbookAdded();
    void addressbookDeleted();

    void requestFailed();

private:
    Settings * m_settings;
    QNetworkAccessManager * m_manager;

    UserAccount * m_account;
    QMap<QString, double> m_exchangeRates;
    QString m_hash;
    QString m_status;

    QString m_captchaImage;
    QString m_captchaToken;

    void sendRequest(const QUrl &url, const Destination &destination) const;
    void receiveReply(QNetworkReply *reply);
    bool getJsonOutcomeValue(const QByteArray &reply, QJsonValue &outcomeValue);

    void processCaptcha(const QByteArray &reply);

    void processUserRegister(const QByteArray &reply);
    void processUserLogIn(const QByteArray &reply);
    void processUserInfo(const QByteArray &reply);
    void processUserEdit(const QByteArray &reply);
    void processUserPay(const QByteArray &reply);
    void processUserMessage(const QByteArray &reply);

    void processExchangeRates(const QByteArray &reply);
    void processPackstations(const QByteArray &reply);

    void processPackageTrack(const QByteArray &reply);

    void processPackageAdd(const QByteArray &reply);
    void processPackageCancel(const QByteArray &reply);
    void processPackagePrice(const QByteArray &reply);
    void processPackageDelete(const QByteArray &reply);
    void processPackageDetails(const QByteArray &reply);

    void processPackagesSent(const QByteArray &reply);
    void processPackagesWaiting(const QByteArray &reply);
    void processPackagesOrder(const QByteArray &reply);

    void processAddressbookList(const QByteArray &reply);
    void processAddressbookAdd(const QByteArray &reply);
    void processAddressbookDelete(const QByteArray &reply);

    bool setDefaultSettings() const;
    bool checkSettingsValidity() const;
};

#endif // CORE_H
