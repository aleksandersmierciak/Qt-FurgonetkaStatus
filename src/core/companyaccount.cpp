#include <companyaccount.h>

CompanyAccount::CompanyAccount(QObject *parent) :
    UserAccount(parent)
{
}

QString CompanyAccount::companyName() const
{
    return this->m_companyName;
}

void CompanyAccount::setCompanyName(const QString &companyName)
{
    this->m_companyName = companyName;
}

QString CompanyAccount::companyNip() const
{
    return this->m_companyNip;
}

void CompanyAccount::setCompanyNip(const QString &companyNip)
{
    this->m_companyNip = companyNip;
}
