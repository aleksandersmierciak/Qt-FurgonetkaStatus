#include <QStringList>

#include <useraccount.h>

UserAccount::UserAccount(QObject *parent) :
    QObject(parent) { }

QString UserAccount::accountType() const
{
    return this->m_accountType;
}

void UserAccount::setAccountType(const QString &accountType)
{
    this->m_accountType = accountType;
}

QString UserAccount::email() const
{
    return this->m_email;
}

void UserAccount::setEmail(const QString &email)
{
    this->m_email = email;
}

QString UserAccount::hashedPassword() const
{
    return this->m_hashedPassword;
}

void UserAccount::setHashedPassword(const QString &hashedPassword)
{
    this->m_hashedPassword = hashedPassword;
}

QString UserAccount::firstName() const
{
    return this->m_firstName;
}

void UserAccount::setFirstName(const QString &firstName)
{
    this->m_firstName = firstName;
}

QString UserAccount::lastName() const
{
    return this->m_lastName;
}

void UserAccount::setLastName(const QString &lastName)
{
    this->m_lastName = lastName;
}

QString UserAccount::address() const
{
    return this->m_address;
}

void UserAccount::setAddress(const QString &address)
{
    this->m_address = address;
}

QString UserAccount::postalCode() const
{
    return this->m_postalCode;
}

void UserAccount::setPostalCode(const QString &postalCode)
{
    this->m_postalCode = postalCode;
}

QString UserAccount::city() const
{
    return this->m_city;
}

void UserAccount::setCity(const QString &city)
{
    this->m_city = city;
}

QString UserAccount::phoneNo() const
{
    return this->m_phoneNo;
}

void UserAccount::setPhoneNo(const QString &phoneNo)
{
    this->m_phoneNo = phoneNo;
}

QString UserAccount::iban() const
{
    return this->m_iban;
}

void UserAccount::setIban(const QString &iban)
{
    this->m_iban = iban;
}

bool UserAccount::commercial() const
{
    return this->m_commercial;
}

void UserAccount::setCommercial(const bool &commercial)
{
    this->m_commercial = commercial;
}

double UserAccount::balance() const
{
    return this->m_balance;
}

void UserAccount::setBalance(const double &balance)
{
    this->m_balance = balance;
}
