#ifndef PACKAGE_H
#define PACKAGE_H

#include <QObject>
#include <QString>

enum class WrappingType;

enum class ShapeType;

class Package : public QObject {
    Q_OBJECT

public:
    Package(QObject *parent = 0);

    Q_PROPERTY(QString m_packageType READ packageType WRITE setPackageType)
    QString packageType() const;
    void setPackageType(const QString &packageType);

    Q_PROPERTY(WrappingType m_wrapping READ wrapping WRITE setWrapping)
    WrappingType wrapping() const;
    void setWrapping(const WrappingType &wrapping);

    Q_PROPERTY(ShapeType m_shape READ shape WRITE setShape)
    ShapeType shape() const;
    void setShape(const ShapeType &shape);

    Q_PROPERTY(int m_weight READ weight WRITE setWeight)
    int weight() const;
    void setWeight(const int &weight);

    Q_PROPERTY(int m_width READ width WRITE setWidth)
    int width() const;
    void setWidth(const int &width);

    Q_PROPERTY(int m_height READ height WRITE setHeight)
    int height() const;
    void setHeight(const int &height);

    Q_PROPERTY(int m_depth READ depth WRITE setDepth)
    int depth() const;
    void setDepth(const int &depth);

    Q_PROPERTY(double m_value READ value WRITE setValue)
    double value() const;
    void setValue(const double &value);

    Q_PROPERTY(bool m_privateShipping READ privateShipping WRITE setPrivateShipping)
    bool privateShipping() const;
    void setPrivateShipping(const bool &privateShipping);

    Q_PROPERTY(QString m_receiverPostalCode READ receiverPostalCode WRITE setReceiverPostalCode)
    QString receiverPostalCode() const;
    void setReceiverPostalCode(const QString &receiverPostalCode);

    Q_PROPERTY(bool m_additionalHandling READ additionalHandling WRITE setAdditionalHandling)
    bool additionalHandling() const;
    void setAdditionalHandling(const bool &additionalHandling);

    Q_PROPERTY(QString m_guarantee READ guarantee WRITE setGuarantee)
    QString guarantee() const;
    void setGuarantee(const QString &guarantee);

    Q_PROPERTY(bool m_rod READ rod WRITE setRod)
    bool rod() const;
    void setRod(const bool &rod);

    Q_PROPERTY(double m_cod READ cod WRITE setCod)
    double cod() const;
    void setCod(const double &cod);

    Q_PROPERTY(bool m_quickTransfer READ quickTransfer WRITE setQuickTransfer)
    bool quickTransfer() const;
    void setQuickTransfer(const bool &quickTransfer);

private:
    QString m_packageType;
    WrappingType m_wrapping;
    ShapeType m_shape;
    int m_weight;
    int m_width;
    int m_height;
    int m_depth;
    double m_value;
    bool m_privateShipping;
    QString m_receiverPostalCode;
    QString m_receiverCountryCode;
    bool m_additionalHandling;
    QString m_guarantee;
    bool m_rod;
    double m_cod;
    bool m_quickTransfer;
};

#endif // PACKAGE_H
