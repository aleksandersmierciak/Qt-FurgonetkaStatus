#ifndef USERACCOUNT_H
#define USERACCOUNT_H

#include <QObject>
#include <QString>

class UserAccount : public QObject
{
    Q_OBJECT
public:
    explicit UserAccount(QObject *parent = 0);
    
    Q_PROPERTY(QString m_accountType READ accountType WRITE setAccountType)
    QString accountType() const;
    void setAccountType(const QString &accountType);

    Q_PROPERTY(QString m_email READ email WRITE setEmail)
    QString email() const;
    void setEmail(const QString &email);

    Q_PROPERTY(QString m_hashedPassword READ hashedPassword WRITE setHashedPassword)
    QString hashedPassword() const;
    void setHashedPassword(const QString &hashedPassword);

    Q_PROPERTY(QString m_firstName READ firstName WRITE setFirstName)
    QString firstName() const;
    void setFirstName(const QString &firstName);

    Q_PROPERTY(QString m_lastName READ lastName WRITE setLastName)
    QString lastName() const;
    void setLastName(const QString &lastName);

    Q_PROPERTY(QString m_address READ address WRITE setAddress)
    QString address() const;
    void setAddress(const QString &address);

    Q_PROPERTY(QString m_postalCode READ postalCode WRITE setPostalCode)
    QString postalCode() const;
    void setPostalCode(const QString &postalCode);

    Q_PROPERTY(QString m_city READ city WRITE setCity)
    QString city() const;
    void setCity(const QString &city);

    Q_PROPERTY(QString m_phoneNo READ phoneNo WRITE setPhoneNo)
    QString phoneNo() const;
    void setPhoneNo(const QString &phoneNo);

    Q_PROPERTY(QString m_iban READ iban WRITE setIban)
    QString iban() const;
    void setIban(const QString &iban);

    Q_PROPERTY(double m_commercial READ commercial WRITE setCommercial)
    bool commercial() const;
    void setCommercial(const bool &commercial);

    Q_PROPERTY(double m_balance READ balance WRITE setBalance)
    double balance() const;
    void setBalance(const double &balance);

private:
    QString m_accountType;
    QString m_email;
    QString m_hashedPassword;
    QString m_firstName;
    QString m_lastName;
    QString m_address;
    QString m_postalCode;
    QString m_city;
    QString m_phoneNo;
    QString m_iban;
    bool m_commercial;
    double m_balance;
};

#endif // USERACCOUNT_H
