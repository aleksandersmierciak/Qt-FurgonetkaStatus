#include <package.h>


enum class WrappingType {
    Cardboard = 0,
    Metal = 1,
    Wood = 2,
    Plastic = 3,
    Rubber = 4,
    Stretch = 5,
    Paper = 6,
    Other = 7
};

enum class ShapeType {
    Regular = 0,
    Irregular = 1
};

/*!
 * \class Package
 * \brief The Package class represents a single package to be sent by Furgonetka service.
 *
 * \inmodule FurgonetkaService
 */

/*!
 * \brief constructs a new instance of the \c Package class
 * This constructor initializes a new instance of the \a Package class
 * and sets its parent as \a parent.
 */
Package::Package(QObject *parent) :
    QObject(parent)
{
}

/*!
 * \property Package::m_packageType
 * \brief the package's type
 *
 * This property describes the type of the package. Three values are valid for
 * the Furgonetka service, each corresponding to the three basic package size types:
 *
 * \value package
 *        Package is of standard size.
 * \value dox
 *        Package is of envelope size.
 * \value pallette
 *        Package is of pallette size.
 */

/*!
 * Returns this package's type.
 *
 * \sa Package::m_packageType, Package::setPackageType()
 */
QString Package::packageType() const
{
    return this->m_packageType;
}

/*!
 * Sets this package's type.
 *
 * \sa Package::m_packageType, Package::packageType()
 */
void Package::setPackageType(const QString &packageType)
{
    this->m_packageType = packageType;
}

/*!
 * \enum WrappingType
 * \brief specifies package wrapping type
 * This enum type specifies package wrapping type:
 *
 * \value Cardboard
 *        Package wrapping is made of cardboard.
 * \value Metal
 *        Package wrapping is made of metal.
 * \value Wood
 *        Package wrapping is made of wood.
 * \value Plastic
 *        Package wrapping is made of plastic.
 * \value Rubber
 *        Package wrapping is made of rubber.
 * \value Stretch
 *        Package wrapping is made of stretch.
 * \value Paper
 *        Package wrapping is made of paper.
 * \value Other
 *        Package wrapping is made of some other material.
 */

/*!
 * \property Package::m_wrapping
 * \brief the package's wrapping type
 *
 * This property describes the type of package wrapping. Possible values
 * are specified by \a WrappingType enumeration.
 *
 * \sa WrappingType, Package::wrapping(), Package::setWrapping()
 */

/*!
 * Returns this package's wrapping type.
 *
 * \sa WrappingType, Package::m_wrapping, Package::setWrapping()
 */
WrappingType Package::wrapping() const
{
    return this->m_wrapping;
}

/*!
 * Sets this package's wrapping type.
 *
 * \sa WrappingType, Package::m_wrapping, Package::wrapping()
 */
void Package::setWrapping(const WrappingType &wrapping)
{
    this->m_wrapping = wrapping;
}

/*!
 * \enum ShapeType
 * \brief specifies package shape type
 * This enum specifies package shape type:
 *
 * \value Regular
 *        Package shape is regular.
 * \value Irregular
 *        Package shape is not regular.
 */

/*!
 * \property Package::m_shape
 * \brief the package's shape
 *
 * This property describes the shape of package. Possible values
 * are specified by \a ShapeType enumeration.
 *
 * \sa ShapeType, Package::wrapping(), Package::setWrapping()
 */

/*!
 * Returns this package's shape type.
 *
 * \sa ShapeType, Package::m_shape, Package::setShape()
 */
ShapeType Package::shape() const
{
    return this->m_shape;
}

/*!
 * Sets this package's shape type.
 *
 * \sa ShapeType, Package::m_shape, Package::shape()
 */
void Package::setShape(const ShapeType &shape)
{
    this->m_shape = shape;
}

/*!
 * \property Package::m_weight
 * \brief the package's weight
 *
 * This property describes the weight of package in kilograms.
 *
 * \sa Package::weight(), Package::setWeight()
 */

/*!
 * Returns this package's weight.
 *
 * \sa Package::m_weight, Package::setWeight()
 */
int Package::weight() const
{
    return this->m_weight;
}

/*!
 * Sets this package's weight.
 *
 * \sa Package::m_weight, Package::weight()
 */
void Package::setWeight(const int &weight)
{
    this->m_weight = weight;
}

/*!
 * \property Package::m_width
 * \brief the package's width
 *
 * This property describes the width of package in centimeters.
 *
 * \sa Package::width(), Package::setWidth()
 */

/*!
 * Returns this package's width.
 *
 * \sa Package::m_width, Package::setWidth()
 */
int Package::width() const
{
    return this->m_width;
}

/*!
 * Sets this package's width.
 *
 * \sa Package::m_width, Package::width()
 */
void Package::setWidth(const int &width)
{
    this->m_width = width;
}

/*!
 * \property Package::m_height
 * \brief the package's height
 *
 * This property describes the height of package in centimeters.
 *
 * \sa Package::height(), Package::setHeight()
 */

/*!
 * Returns this package's height.
 *
 * \sa Package::m_height, Package::setHeight()
 */
int Package::height() const
{
    return this->m_height;
}

/*!
 * Sets this package's height.
 *
 * \sa Package::m_height, Package::height()
 */
void Package::setHeight(const int &height)
{
    this->m_height = height;
}

/*!
 * \property Package::m_depth
 * \brief the package's depth
 *
 * This property describes the depth of package in centimeters.
 *
 * \sa Package::depth(), Package::setDepth()
 */

/*!
 * Returns this package's depth.
 *
 * \sa Package::m_depth, Package::setDepth()
 */
int Package::depth() const
{
    return this->m_depth;
}

/*!
 * Sets this package's money value for insurance purposes.
 *
 * \sa Package::m_depth, Package::depth()
 */
void Package::setDepth(const int &depth)
{
    this->m_depth = depth;
}

/*!
 * \property Package::m_value
 * \brief the package's value
 *
 * This property describes the value of package in Polish zlotys (PLN).
 *
 * \sa Package::value(), Package::setValue()
 */

/*!
 * Returns this package's money value for insurance purposes.
 *
 * \sa Package::m_value, Package::setValue()
 */
double Package::value() const
{
    return this->m_value;
}

/*!
 * Sets this package's money value for insurance purposes.
 *
 * \sa Package::m_value, Package::value()
 */
void Package::setValue(const double &value)
{
    this->m_value = value;
}

/*!
 * \property Package::m_privateShipping
 * \brief whether this package is to be shipped to private person
 *
 * This property describes whether this package is to be shipped
 * to private person. UPS delivery service only.
 *
 * \sa Package::privateShipping(), Package::setPrivateShipping()
 */

/*!
 * Returns \c true if this package is to be shipped to private person;
 * \c false otherwise. UPS delivery service only.
 *
 * \sa Package::m_privateShipping, Package::setPrivateShipping()
 */
bool Package::privateShipping() const
{
    return this->m_privateShipping;
}

/*!
 * Sets whether this package is to be shipped to private person according to \a privateShipping.
 *
 * \sa Package::m_privateShipping, Package::privateShipping()
 */
void Package::setPrivateShipping(const bool &privateShipping)
{
    this->m_privateShipping = privateShipping;
}

/*!
 * \property Package::m_receiverPostalCode
 * \brief the package receiver's postal code
 *
 * This property describes the package receiver's postal code.
 *
 * \sa Package::receiverPostalCode(), Package::setReceiverPostalCode()
 */

/*!
 * Returns the package receiver's postal code.
 *
 * \sa Package::m_receiverPostalCode, Package::setReceiverPostalCode()
 */
QString Package::receiverPostalCode() const
{
    return this->m_receiverPostalCode;
}

/*!
 * Sets the package receiver's postal code.
 *
 * \sa Package::m_receiverPostalCode, Package::receiverPostalCode()
 */
void Package::setReceiverPostalCode(const QString &receiverPostalCode)
{
    this->m_receiverPostalCode = receiverPostalCode;
}

/*!
 * \property Package::m_additionalHandling
 * \brief whether this package needs additional handling
 *
 * This property describes whether this package needs additional handling.
 * UPS delivery service only.
 *
 * \sa Package::additionalHandling(), Package::setAdditionalHandling()
 */

/*!
 * Returns \c true if this package needs additional handling;
 * \c false otherwise. UPS delivery service only.
 *
 * \sa Package::m_additionalHandling, Package::setAdditionalHandling()
 */
bool Package::additionalHandling() const
{
    return this->m_additionalHandling;
}

/*!
 * Sets whether this package needs additional handling. UPS delivery service only.
 *
 * \sa Package::m_additionalHandling, Package::additionalHandling()
 */
void Package::setAdditionalHandling(const bool &additionalHandling)
{
    this->m_additionalHandling = additionalHandling;
}

/*!
 * \property Package::m_guarantee
 * \brief the package's time of arrival guarantee
 *
 * This property describes the package's time of arrival. Recognized values:
 * \value 0930
 *        The package is to be delivered until 0930 in the morning.
 * \value 1200
 *        The package is to be delivered until 1200 in the morning.
 * \value Sat
 *        The package is to be delivered on Saturday. UPS delivery service only,
 *        requires specifying \a receiverPostalCode.
 *
 * \sa Package::guarantee(), Package::setGuarantee()
 */

/*!
 * Returns time restrictions for the package delivery.
 *
 * \sa Package::m_guarantee, Package::setGuarantee()
 */
QString Package::guarantee() const
{
    return this->m_guarantee;
}

/*!
 * Sets time restrictions for the package delivery.
 *
 * \sa Package::m_guarantee, Package::guarantee()
 */
void Package::setGuarantee(const QString &guarantee)
{
    this->m_guarantee = guarantee;
}

/*!
 * \property Package::m_rod
 * \brief whether a status receipt is to be sent
 *
 * This property describes whether a status receipt is to be sent.
 *
 * \sa Package::rod(), Package::setRod()
 */

/*!
 * Returns \c true if status receipt is to be sent; \c false otherwise.

 * \sa Package::m_rod, Package::setRod()
 */
bool Package::rod() const
{
    return this->m_rod;
}

/*!
 * Sets whether status receipt will be sent.

 * \sa Package::m_rod, Package::rod()
 */
void Package::setRod(const bool &rod)
{
    this->m_rod = rod;
}

/*!
 * \property Package::m_cod
 * \brief the money amount to be retrieved upon delivery
 *
 * This property describes the money amount in Polish Zlotys (PLN)
 * to be retrieved upon delivery if cash on delivery was selected.
 *
 * \sa Package::cod(), Package::setCod()
 */

/*!
 * Returns the money amount to be retrieved upon delivery, if cash on delivery was selected.
 *
 * \sa Package::m_cod, Package::setCod()
 */
double Package::cod() const
{
    return this->m_cod;
}

/*!
 * Sets the money amount to be retrieved upon delivery, if cash on delivery was selected.
 *
 * \sa Package::m_cod, Package::cod()
 */
void Package::setCod(const double &cod)
{
    this->m_cod = cod;
}

/*!
 * \property Package::m_quickTransfer
 * \brief whether Express DPD service is to be used
 *
 * This property describes whether Express DPD service is to be used.
 *
 * \sa Package::quickTransfer(), Package::setQuickTransfer()
 */

/*!
 * Returns \c true if Express DPD service is to be used; \c false otherwise.
 *
 * \sa Package::m_quickTransfer, Package::setQuickTransfer()
 */
bool Package::quickTransfer() const
{
    return this->m_quickTransfer;
}

/*!
 * Sets whether Express DPD service is to be used.
 *
 * \sa Package::m_quickTransfer, Package::quickTransfer()
 */
void Package::setQuickTransfer(const bool &quickTransfer)
{
    this->m_quickTransfer = quickTransfer;
}
