TARGET = core
TEMPLATE = lib
CONFIG += staticlib

QT += network

DEFINES += QT_NO_CAST_FROM_ASCII \
    QT_NO_CAST_TO_ASCII

CONFIG += c++11

SOURCES += companyaccount.cpp \
           core.cpp \
           package.cpp \
           useraccount.cpp

HEADERS += companyaccount.h \
           core.h \
           package.h \
           settings.h \
           useraccount.h


unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
