#ifndef COMPANYACCOUNT_H
#define COMPANYACCOUNT_H

#include "useraccount.h"

class CompanyAccount : public UserAccount
{
    Q_OBJECT
public:
    explicit CompanyAccount(QObject *parent = 0);

    Q_PROPERTY(QString m_companyName READ companyName WRITE setCompanyName)
    QString companyName() const;
    void setCompanyName(const QString &companyName);

    Q_PROPERTY(QString m_companyNip READ companyNip WRITE setCompanyNip)
    QString companyNip() const;
    void setCompanyNip(const QString &companyNip);

private:
    QString m_companyName;
    QString m_companyNip;
};

#endif // COMPANYACCOUNT_H
