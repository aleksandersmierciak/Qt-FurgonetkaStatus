#include <QString>
#include <QtTest>

class CoreTest : public QObject
{
    Q_OBJECT
    
public:
    CoreTest();
    
private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testCase1();
    void testCase1_data();
};

CoreTest::CoreTest()
{
}

void CoreTest::initTestCase()
{
}

void CoreTest::cleanupTestCase()
{
}

void CoreTest::testCase1()
{
    QFETCH(QString, data);
    QVERIFY2(true, "Failure");
}

void CoreTest::testCase1_data()
{
    QTest::addColumn<QString>("data");
    QTest::newRow("0") << QString();
}

QTEST_APPLESS_MAIN(CoreTest)

#include "tst_coretest.moc"
